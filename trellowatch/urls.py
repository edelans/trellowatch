from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from . import views
admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^watcher/', include('watcher.urls')),
    url(r'^$', views.home, name='home'),
    url('', include('social_django.urls', namespace='social')),
    url('', include('django.contrib.auth.urls', namespace='auth')),

]

# django debu toolbar config
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
