# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-08 20:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('watcher', '0006_board_webhook_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='board',
            name='idOrganization',
            field=models.CharField(default='', max_length=25),
            preserve_default=False,
        ),
    ]
