import requests


def add_label_to_card(card_id):
    endpoint = "https://api.trello.com/1/cards/"
    endpoint += card_id
    endpoint += '/labels'
    params = {
        "name": "logged",
        "key": APP_KEY,
        "token": TOKEN,
        "color": "green"
    }

    r = requests.post(endpoint, params=params)
    if r.status_code == 200:
        return True
    return r
