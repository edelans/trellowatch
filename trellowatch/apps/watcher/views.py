from django.http import HttpResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.conf import settings

from ipaddress import ip_address, ip_network
import json, requests

from django.contrib.auth.models import User
from .models import Board, TrelloTask

import logging
logger = logging.getLogger("trellowatch")

@login_required
def board_list(request):
    if not request.user.social_auth.filter(provider='trello'):
        messages.error(request, 'You must be logged in with trello to access this page.')
        return redirect('/')
    board_list = list(Board.objects.filter(user=request.user).order_by('name'))
    context = {
        'board_list': board_list,
    }
    return render(request, 'watcher/board_list.html', context)


@login_required
def trello_task(request):
    if not request.user.social_auth.filter(provider='trello'):
        messages.error(request, 'You must be logged in with trello to access this page.')
        return redirect('/')
    trello_task = list(TrelloTask.objects.filter(user=request.user).filter(is_awaiting_reply=True))
    context = {
        'trello_task': trello_task,
    }
    return render(request, 'watcher/trello_task.html', context)


@login_required
def generate_board_list(request):
    if not request.user.social_auth.filter(provider='trello'):
        messages.error(request, 'You must be logged in with trello to access this page.')
        return redirect('/')

    social_user = request.user.social_auth.get(provider='trello')
    token = social_user.access_token['oauth_token']
    req = requests.get("https://api.trello.com/1/members/me/boards?key="+settings.SOCIAL_AUTH_TRELLO_KEY+"&token="+token)
    boards = req.json()

    created = 0
    for board in req.json():
        try:
            obj = Board.objects.get(
                board_trello_id=board['id'],
                user=request.user,
            )
            if obj.name != board['name']:
                obj.name = board['name']
                obj.save()
            if obj.idOrganization != board['idOrganization']:
                obj.idOrganization = board['idOrganization'] if board['idOrganization'] else ''
                obj.save()

        except Board.DoesNotExist:
            obj = Board(
                board_trello_id=board['id'],
                name=board['name'],
                user=request.user,
                idOrganization=board['idOrganization'] if board['idOrganization'] else '',
            )
            obj.save()
            created +=1

    messages.success(request, "Board list updated : {} new boards created".format(created))
    return redirect('watcher:board_list')

@login_required
def mute_this_task(request, task_id):
    task = TrelloTask.objects.get(id=task_id)
    task.is_awaiting_reply = False
    task.save()
    return redirect('watcher:trello_task')

@login_required
def update_watched_boards(request):
    logger.info('entered update_watched_boards')
    for key in request.POST:
        if "toggle_" in key:
            logger.debug("key: "+key)
            board_id = key.split("_")[1]
            board = get_object_or_404(Board, board_trello_id=board_id, user=request.user)

            social_user = request.user.social_auth.get(provider='trello')
            token = social_user.access_token['oauth_token']
            logger.debug("token"+token)

            if board.is_watched != (request.POST[key] == 'on'):
                # Submitted board status is different from db board status. 2 possibilities :
                #  - board.is_watched is False and request.POST[key] is on  -> we should setup the watch
                #  - board.is_watched is True  and request.POST[key] is off -> we should unset the watch
                logger.debug("The status of board \'{}\' has chanded".format(board.name))

                if not board.is_watched:
                    # we should setup the watch
                    logger.info("gonna post a new webhook!")
                    callbackURL = 'http://'+settings.CALLBACK_HOST+'/watcher/trelloCallback/'+str(request.user.id)
                    logger.debug('callbackURL : '+callbackURL)
                    parameters = {
                        'description': 'Webhook for trellowbeb for board {}'.format(board.name),
                        'callbackURL': callbackURL,
                        'idModel': board_id
                    }
                    post_url = "https://api.trello.com/1/webhooks/?key="+settings.SOCIAL_AUTH_TRELLO_KEY+"&token="+token
                    logger.debug("post_url: "+post_url)
                    r = requests.post(post_url, data=parameters)
                    logger.debug("post request response: "+r.text)
                    if r.status_code == 200:
                        board.webhook_id = r.json()['id']
                        board.is_watched = (request.POST[key] == 'on')
                        board.save()
                        messages.success(request, 'New settings were saved.')
                    else :
                        messages.success(request, 'There was a problem with your request.')



                else:
                    # we should unset the watch
                    logger.info("gonna destroy an existing webhook")
                    webhook_id = board.board_trello_id
                    logger.debug("webhook id is : {}".format(webhook_id))
                    r = requests.delete("https://api.trello.com/1/webhooks/"+webhook_id+"?key="+settings.SOCIAL_AUTH_TRELLO_KEY+"&token="+token)
                    logger.debug("deletion request response: " + r.text)
                    board.webhook_id = ""
                    board.is_watched = (request.POST[key] == 'on')
                    board.save()
                    messages.success(request, 'New settings were saved.')

    return redirect('watcher:board_list')


def check_request_is_from_trello(forwarded_for):
    client_ip_address = ip_address(forwarded_for)
    logger.debug('client_ip_address: {}'.format(client_ip_address))
    whitelist = [
        "107.23.104.115",
        "107.23.149.70",
        "54.152.166.250",
        "54.164.77.56",
    ]

    for valid_ip in whitelist:
        if client_ip_address in ip_network(valid_ip):
            logger.info("Request from Trello IP")
            break
    else:
        logger.info("Request from Unknown IP")
        return False

    return True


@csrf_exempt
def trello_webhook(request, user_id):
    if request.method == "HEAD":
        return HttpResponse("OK")

    forwarded_for = u'{}'.format(request.META.get('HTTP_X_FORWARDED_FOR'))
    logger.debug('forwarded_for: '+forwarded_for)
    if not check_request_is_from_trello(forwarded_for):
        logger.info("Access Denied, not from Trello")
        return HttpResponseForbidden("Access Denied, not from Trello")

    obj = request.body.decode("utf-8")
    data = json.loads(obj)
    action = data["action"]

    logger.debug('user_id: '+user_id)
    user = User.objects.get(id=user_id)
    username = user.social_auth.get(provider='trello').extra_data['username']
    logger.debug('trello username: ' +username)

    if action["type"] == "commentCard":
        logger.debug("Commented card")
        if username in action["data"]["text"]:
            logger.debug("ACHTUNG Mentionned me!")
            try:
                task = TrelloTask.objects.filter(user=user).get(trello_card_id=action["data"]["card"]["id"])
                task.accumulated_comments_since_my_last_action +=1
                task.is_awaiting_reply = True
                task.save()
            except (KeyError, TrelloTask.DoesNotExist):
                task = TrelloTask.objects.create(
                    user=user,
                    trello_card_id=action["data"]["card"]["id"],
                    board_name = action["data"]["board"]["name"],
                    title = action["data"]["card"]["name"],
                    author = action["memberCreator"]["username"],
                    accumulated_comments_since_my_last_action = 1,
                    is_awaiting_reply = True,
                )

        elif action["memberCreator"]["username"] == username:
            task = get_object_or_404(TrelloTask, trello_card_id=action["data"]["card"]["id"], user=user)
            task.is_awaiting_reply = False
            task.accumulated_comments_since_my_last_action = 0
            task.save()
        else:
            task = get_object_or_404(TrelloTask, trello_card_id=action["data"]["card"]["id"], user=user)
            task.accumulated_comments_since_my_last_action+=1
            task.save()

    return HttpResponse(status=200)
