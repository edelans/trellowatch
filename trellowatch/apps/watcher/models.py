from django.conf import settings
from django.db import models
from django.utils import timezone
# Create your models here.

class TrelloTask(models.Model):
    """
    Model used to log cards awaiting replies
    """
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    board_name = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=50)
    trello_card_id = models.CharField(max_length=30)
    last_activity = models.DateTimeField(default=timezone.now)
    accumulated_comments_since_my_last_action = models.IntegerField(default=0)
    is_awaiting_reply = models.BooleanField(default=True)

    def __str__(self):
        return self.title

class Board(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    board_trello_id = models.CharField(max_length=25)
    name = models.CharField(max_length=255)
    idOrganization = models.CharField(max_length=25)
    is_watched = models.BooleanField(default=False)
    webhook_id = models.CharField(max_length=25)

    def get_url(self):
        return "https://trello.com/b/" + self.board_id

    def __str__(self):
        return self.name
