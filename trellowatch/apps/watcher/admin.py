from django.contrib import admin
from .models import TrelloTask, Board

# Register your models here.

admin.site.register(TrelloTask)
admin.site.register(Board)
