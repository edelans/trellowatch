from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from . import views

app_name = "watcher"

urlpatterns = [
    url(r'^board_list/$', views.board_list, name='board_list'),
    url(r'^trello_task/$', views.trello_task, name='trello_task'),
    url(r'^update_watched_boards/$', views.update_watched_boards, name='update_watched_boards'),
    url(r'^generate_board_list/$', views.generate_board_list, name='generate_board_list'),
    url(r'^mute_this_task/(?P<task_id>\w+)', views.mute_this_task, name='mute_this_task'),
    url(r'^trelloCallback/(?P<user_id>\w+)', views.trello_webhook, name='trello_webhook'),
]
