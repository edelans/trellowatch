from django.shortcuts import render

import logging
logger = logging.getLogger("trellowatch")

def home(request):
    context = {
        'request': request,
        'user': request.user
    }

    # if request.user.is_authenticated:
    #     user = request.user
        # social_user = request.user.social_auth.get(provider='trello')
        # logger.debug(social_user.access_token)
    return render(request, 'home.html', context)
