git@gitlab.com:edelans/trellowatch.git

template used : https://github.com/pydanny/cookiecutter-django


# Settings

The settings folder is a python module. The module is loaded by the `manage.py` script and the `wsgi.py` script, according to the variable `DJANGO_SETTINGS_MODULE`, which is set by default to `trellowatch.settings` (which includes `base.py` and `local.py` files) if no environment variable is found.

To set an environment variable :

```
export DJANGO_SETTINGS_MODULE="trellowatch.settings.production"
export DJANGO_SETTINGS_MODULE="trellowatch.settings.local"
```

The standard way for permanently store global environment variables (system wide) is to add an entry in `/etc/environment`

NB: beware, env variables must be defined in the wsgi.py file.

# Server configuration

https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-16-04




# Deploy oneliner
```git push live master```
+ lancer :
sudo systemctl restart trellowatch


Pour ajouter la remote '```live```' :
        git remote add live ssh://deploy@eiffel:/var/repo/trellowatch.git

# Git hook

dans `/var/repo/trellowatch.git/hooks/post-receive` :
```
#!/bin/bash
set -eu

TARGET="/var/www/trellowatch"
GIT_DIR="/var/repo/trellowatch.git"
BRANCH="master"

while read oldrev newrev ref
do
        # only checking out the master (or whatever branch you would like to deploy)
        if [[ $ref = refs/heads/"$BRANCH" ]];
        then
                echo "Ref $ref received. Deploying ${BRANCH} branch to production..."
                git --work-tree="$TARGET" --git-dir="$GIT_DIR" checkout -f "${BRANCH}"
                cd "$TARGET"
                sh post-receive.sh
        else
                echo "Ref $ref received. Doing nothing: only the ${BRANCH} branch may be deployed on this server."
        fi
done

```

# Virtualenv
use virtualenvwrapper :
```
mkvirtualenv --python=/usr/bin/python3 trellowatch
```

and then `workon` command.


# use systemd
systemd is an init system used manage processes, like gunicorn. It will launch gunicorn at server start, and respawn it when it fails.

## Configuration
A service configuration file is provided at the root of this repo. It should be stored in `/etc/systemd/system/`.

## Logs consultation
Make use of `journalctl`. It will show the logs of gunicorn, which receive the logs Django sends to the console (depending on you logger config).

You can filter by :

- time : `journalctl --since 09:00 --until "1 hour ago"`, `journalctl --since "2015-01-10" --until "2015-01-11 03:00"`, `journalctl --since yesterday --until today`

- by service : `journalctl -u nginx.service`, journalctl -u trellowatch.service

- as you go (like `tail -f`) : `journalctl -f`

## Logs cleaning

- to keep only 1G of logs : `sudo journalctl --vacuum-size=1G`
- to keep entries from the last year : `sudo journalctl --vacuum-time=1years`
